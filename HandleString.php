<?php

    class HandleString {

        // khai bao thuoc tinh
        private $check1;
        private $check2;

        // phuong thuc readFile() de doc cac file.txt
        public function readFile($file) {
            $data = fopen($file, "r");
            if (!$data) {
                return false;
            } else {
                $str = fread($data, filesize($file));
                if ($str) return $str;
            }
        }
       
        // function kiem tra chuoi
        public function checkValdString($str) {
            if (strlen($str)==0) {
                return true;
            } elseif (strlen($str)>50 && substr_count($str,"after")==0) {
                return true;               
            } elseif (substr_count($str,"before")!=0) {
                return true;      
            } else {
                return false;       
            }
        }
    }

    // Khoi tao doi tuong object1
    $object1 = new HandleString();

    $check1 = $object1->readFile("file1.txt");

    if ($check1) {
        echo "Doc file thanh cong. \n";
        $check1 = $object1 -> checkValdString($check1);
        echo "Gia tri cua check1 la : ";
        var_dump($check1);
    } else {
        echo "Doc file khong thanh cong ";
    }

    echo "<br>";

    $check2 = $object1->readFile("file2.txt");
    
    if ($check2) {
        echo "Doc file thanh cong. \n";
        $check2 = $object1 -> checkValdString($check2);
        echo "Gia tri cua check2 la : ";        
        var_dump($check2);
    } else {
        echo "Doc file khong thanh cong " ."<br>";
    }
?>
